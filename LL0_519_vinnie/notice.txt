 ID: LL0_519_vinnie_dataset
 Name: LL0_519_vinnie
 Description: **Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Following are data on the shooting of Vinnie Johnson of the Detroit
Pistons during the 1985-1986 through 1988-1989 seasons. Source was the
New York Times.
The data are analyzed in the Carnegie Mellon Ph.D. Thesis of
Kate Hsiao and some results are cited in Example 2 of Kass, R.E. and
Raftery, A.E. (1995), Bayes Factors, J. Amer. Statist. Assoc.,
The first column is the year, with 85 indicating 1985-1986, etc..
The second column is Field Goals, the third column is Field Goal
Attempts.
A more complete version of the data, including free throws, is
appended together with additional information.


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific
 License: CC-BY License
 License Link: http://creativecommons.org/licenses/by/4.0/
 Source: OpenML
 Source Link: http://www.openml.org/d/519
 Citation: 
    @article{OpenML2013,
    author = {Vanschoren, Joaquin and van Rijn, Jan N. and Bischl, Bernd and Torgo, Luis},
    title = {OpenML: Networked Science in Machine Learning},
    journal = {SIGKDD Explorations},
    volume = {15},
    number = {2},
    year = {2013},
    pages = {49--60},
    url = {http://doi.acm.org/10.1145/2641190.2641198},
    doi = {10.1145/2641190.2641198},
    publisher = {ACM},
    address = {New York, NY, USA},
    }
