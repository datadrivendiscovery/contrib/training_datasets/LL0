 ID: LL0_721_pwLinear_dataset
 Name: pwLinear
 Description: **Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Binarized version of the original data set (see version 1). It converts the numeric target feature to a two-class nominal target feature by computing the mean and classifying all instances with a lower target value as positive ('P') and all others as negative ('N').
 License: CC-BY License
 License Link: http://creativecommons.org/licenses/by/4.0/
 Source: OpenML
 Source Link: http://www.openml.org/d/721
 Citation: 
    @article{OpenML2013,
    author = {Vanschoren, Joaquin and van Rijn, Jan N. and Bischl, Bernd and Torgo, Luis},
    title = {OpenML: Networked Science in Machine Learning},
    journal = {SIGKDD Explorations},
    volume = {15},
    number = {2},
    year = {2013},
    pages = {49--60},
    url = {http://doi.acm.org/10.1145/2641190.2641198},
    doi = {10.1145/2641190.2641198},
    publisher = {ACM},
    address = {New York, NY, USA},
    }
