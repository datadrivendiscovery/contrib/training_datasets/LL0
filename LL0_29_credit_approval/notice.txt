 ID: LL0_29_credit_approval_dataset
 Name: LL0_29_credit_approval
 Description: **Author**: Confidential - Donated by Ross Quinlan   
**Source**: [UCI](http://archive.ics.uci.edu/ml/datasets/credit+approval) - 1987  
**Please cite**: [UCI](http://archive.ics.uci.edu/ml/citation_policy.html)  

**Credit Approval**
This file concerns credit card applications. All attribute names and values have been changed to meaningless symbols to protect the confidentiality of the data.  
   
This dataset is interesting because there is a good mix of attributes -- continuous, nominal with small numbers of values, and nominal with larger numbers of values.  There are also a few missing values.
 License: CC-BY License
 License Link: http://creativecommons.org/licenses/by/4.0/
 Source: OpenML
 Source Link: http://www.openml.org/d/29
 Citation: 
    @article{OpenML2013,
    author = {Vanschoren, Joaquin and van Rijn, Jan N. and Bischl, Bernd and Torgo, Luis},
    title = {OpenML: Networked Science in Machine Learning},
    journal = {SIGKDD Explorations},
    volume = {15},
    number = {2},
    year = {2013},
    pages = {49--60},
    url = {http://doi.acm.org/10.1145/2641190.2641198},
    doi = {10.1145/2641190.2641198},
    publisher = {ACM},
    address = {New York, NY, USA},
    }
