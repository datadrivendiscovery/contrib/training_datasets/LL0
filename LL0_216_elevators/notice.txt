 ID: LL0_216_elevators_dataset
 Name: LL0_216_elevators
 Description: **Author**:   
**Source**: Unknown -   
**Please cite**:   

This data set is also obtained from the task of controlling a F16
 aircraft, although the target variable and attributes are different
 from the ailerons domain. In this case the goal variable is related to
 an action taken on the elevators of the aircraft.

 Characteristics: 8752 cases, 19 continuous attributes 
 Source: collection of regression datasets by Luis Torgo (ltorgo@ncc.up.pt) at
 http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
 Original source: Experiments of Rui Camacho (rcamacho@garfield.fe.up.pt).
 License: CC-BY License
 License Link: http://creativecommons.org/licenses/by/4.0/
 Source: OpenML
 Source Link: http://www.openml.org/d/216
 Citation: 
    @article{OpenML2013,
    author = {Vanschoren, Joaquin and van Rijn, Jan N. and Bischl, Bernd and Torgo, Luis},
    title = {OpenML: Networked Science in Machine Learning},
    journal = {SIGKDD Explorations},
    volume = {15},
    number = {2},
    year = {2013},
    pages = {49--60},
    url = {http://doi.acm.org/10.1145/2641190.2641198},
    doi = {10.1145/2641190.2641198},
    publisher = {ACM},
    address = {New York, NY, USA},
    }
