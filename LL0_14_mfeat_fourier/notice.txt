 ID: LL0_14_mfeat_fourier_dataset
 Name: mfeat_fourier
 Description: **Author**: Robert P.W. Duin, Department of Applied Physics, Delft University of Technology  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Multiple+Features) - 1998  
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)   

**Multiple Features Dataset: Fourier**  
One of a set of 6 datasets describing features of handwritten numerals (0 - 9) extracted from a collection of Dutch utility maps. Corresponding patterns in different datasets correspond to the same original character. 200 instances per class (for a total of 2,000 instances) have been digitized in binary images. 

### Attribute Information  
The attributes represent 76 Fourier coefficients of the character shapes.

### Relevant Papers  
A slightly different version of the database is used in  
M. van Breukelen, R.P.W. Duin, D.M.J. Tax, and J.E. den Hartog, Handwritten digit recognition by combined classifiers, Kybernetika, vol. 34, no. 4, 1998, 381-386.
 
The database as is is used in:  
A.K. Jain, R.P.W. Duin, J. Mao, Statistical Pattern Recognition: A Review, IEEE Transactions on Pattern Analysis and Machine Intelligence archive, Volume 22 Issue 1, January 2000
 License: CC-BY License
 License Link: http://creativecommons.org/licenses/by/4.0/
 Source: OpenML
 Source Link: http://www.openml.org/d/14
 Citation: 
    @article{OpenML2013,
    author = {Vanschoren, Joaquin and van Rijn, Jan N. and Bischl, Bernd and Torgo, Luis},
    title = {OpenML: Networked Science in Machine Learning},
    journal = {SIGKDD Explorations},
    volume = {15},
    number = {2},
    year = {2013},
    pages = {49--60},
    url = {http://doi.acm.org/10.1145/2641190.2641198},
    doi = {10.1145/2641190.2641198},
    publisher = {ACM},
    address = {New York, NY, USA},
    }
